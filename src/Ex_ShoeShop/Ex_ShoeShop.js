import React, { Component } from 'react'
import CartShoe from './CartShoe';

import { dataShoe } from "./dataShoe";
import DetailShoe from './DetailShoe';
import ListShoe from './ListShoe';

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };
  handleaddtoCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id
    });
    if (index == -1) {
      let itemCart = { ...shoe, number: 1 };
      cloneCart.push(itemCart)
    } else {
      cloneCart[index].number++;
    };
    this.setState({
      cart: cloneCart,
    });
    console.log(cloneCart);
  };
  miniusnumberCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id
    })
    if (shoe.number>0){
      cloneCart[index].number = cloneCart[index].number -1;
    }
    this.setState({
      cart:cloneCart,
    })
  }

  // miniusnumberCart = (shoe) => {

  // };
  render() {
    return (
      <div className='container'>
        {/* cart */}
        <CartShoe
          cart={this.state.cart}
          handleaddtoCart={this.handleaddtoCart}
          miniusnumberCart={this.miniusnumberCart}
        />
        {/* card */}
        <ListShoe
          shoeArr={this.state.shoeArr}
          handleChangeDetail={this.handleChangeDetail}
          handleaddtoCart={this.handleaddtoCart}
        />
        {/* detail */}
        <DetailShoe detail={this.state.detail} />
      </div>
    )
  }
}
