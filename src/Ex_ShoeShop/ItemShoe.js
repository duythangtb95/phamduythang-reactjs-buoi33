import React, { Component } from 'react'

export default class ItemShoe extends Component {
    render() {
        let { image, name } = this.props.data
        return (
            <div className="col-3 p-1">
                <div className="card text-left h-100">
                    <img className="card-img-top" src={image} alt />
                    <div className="card-body">
                        <h6 className="card-title">{name}</h6>
                        <div className='d-flex justify-content-between'>
                            <button
                                onClick={() => { this.props.handleaddtoCart(this.props.data); }}
                                className='btn btn-success'>
                                Buy
                            </button>
                            <button
                                onClick={() => { this.props.handleChangeDetail(this.props.data); }}
                                className='btn btn-primary'>Detail</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
