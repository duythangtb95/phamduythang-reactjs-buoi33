import React, { Component } from 'react'

export default class DetailShoe extends Component {
  render() {
    return (
      <div className="row my-5 border border-success">
        <img src={this.props.detail.image} className="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|} col-3" alt />
        <div className='col-9 m-auto'>
          <h1>{this.props.detail.name}</h1>
          <p>{this.props.detail.description}</p>
          <p>{this.props.detail.shortDescription}</p>
          <h1>Price: {this.props.detail.price}$</h1>
          <h1>Quantity: {this.props.detail.quantity}</h1>
        </div>
      </div>

    )
  }
}

