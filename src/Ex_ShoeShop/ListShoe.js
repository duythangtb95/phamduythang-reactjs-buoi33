import React, { Component } from 'react'
import ItemShoe from './ItemShoe';

export default class ListShoe extends Component {
    renderListShoe = () => {
        return this.props.shoeArr.map((item) => {
            return (
            <ItemShoe data={item}
            handleChangeDetail = {this.props.handleChangeDetail}
            handleaddtoCart = {this.props.handleaddtoCart}
            />);
        })
    };
    render() {
        return (
            <div className='row'>
                {this.renderListShoe()}
            </div>
        );
    }
}
