import React, { Component } from 'react'

export default class CartShoe extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return (
                <tr>
                    <td scope="row">{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price * item.number}</td>
                    <td>
                        <button
                            onClick={() => { return this.props.miniusnumberCart(item) }}
                            className='btn btn-primary mx-2'>-</button>
                        {item.number}
                        <button
                            onClick={() => { return this.props.handleaddtoCart(item) }}
                            className='btn btn-primary mx-2'>+</button>
                    </td>
                    <td>
                        <img style={{ width: "80px" }} src={item.image} alt />
                    </td>
                </tr>
            );
        }
        );
    };
    render() {
        return (
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTbody()}
                </tbody>
            </table>
        );
    }
}
